from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='home-map'),  #mappa coi punti
    path('prova/', views.prova,name='prova') #dati
]